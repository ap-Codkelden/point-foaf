import requests

class Error(Exception):
    pass


class PointAPIError(Error):
    def __init__(self, error):
        self.message = "Error occured with HTTP status code {0} and message '{1}'".format(error['code'],error['message'])


    def __str__(self):
        return self.message


class PointAPI(object):
    def __init__(self, user, password):
        self.API='http://point.im/api/'
        self.user = user 
        self.password = password
        self.session = requests.Session()
        self.cookie = None


    def login(self):
        try:
            login_values = {'login' : self.user, 'password' : self.password} 
            req = requests.request('POST', "https://point.im/api/login", data=login_values)
            self.cookie = {'user': req.json()['token']}
            print(self.cookie)
            print('Logged in.')
            return 0
        except:
            return 1


    def api_request(self, url, cookies=None, schema='https://', point='point.im/',params=None):
        """
        Принимает параметр url без ведущего слеша
        """
        if not cookies:
            cookies = self.cookie
        url = ''.join( [ schema, point, url ] )
        #print(url)
        return requests.request('GET', url=url, cookies=cookies, params=params).json()


    def GetUser(self, user_login):
        try:
            url = 'api/user/{0}'.format(user_login)
            #print("URL: ", url)
            response = self.api_request(url)
            if 'code' in response:
                raise PointAPIError(response)
            #print('response: ',[ response ])
            return [ response ]
        except:
            raise
        

    def GetSubscriptions(self, user_login):
        try:
            url = '{0}user/{1}/subscriptions'.format(self.API, user_login)
            print("URL: ", url)
            response = self.api_request(url)
            if 'code' in response:
                raise PointAPIError(response)
            return response
        except:
            raise


    def GetSubscribers(self, user_login):
        try:
            url = '{0}user/{1}/subscribers'.format(self.API, user_login)
            print("URL: ", url)
            response = self.api_request(url)
            if 'code' in response:
                raise PointAPIError(response)
            return response
        except:
            raise